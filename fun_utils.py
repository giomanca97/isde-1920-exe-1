import numpy as np
from pandas import read_csv
import matplotlib.pyplot as plt
from sklearn.metrics.pairwise import euclidean_distances


def load_mnist(csv_filename=r'data/mnist_data.csv'):
    """Load the MNIST dataset."""
    mnist = read_csv(csv_filename)
    mnist = np.array(mnist)
    y = mnist[:, 0]  # get the labels from the first column
    x = mnist[:, 1:]  # get the data from the other columns
    return x, y



def split_data(x, y, fract_tr=0.5):
    """Split the data X,y into two random subsets."""
    num_samples = y.size  # number of samples in data, equal to x.shape[0]
    n_tr = int(fract_tr * num_samples)
    n_ts = num_samples - n_tr
    # create a vector of indexes (ordered integer vector): [0, 1, 2, ...]
    idx = np.arange(num_samples)
    # shuffle it
    np.random.shuffle(idx)
    # extract training/testing idx from the shuffled idx vector
    idx_tr = idx[:n_tr]
    idx_ts = idx[n_tr:]
    assert (n_ts == idx_ts.size)  # assert is useful in unit testing
    # 5) extract training/testing samples from x (along with their labels y)
    x_tr = x[idx_tr, :]
    y_tr = y[idx_tr]
    x_ts = x[idx_ts, :]
    y_ts = y[idx_ts]
    return x_tr, x_ts, y_tr, y_ts



class NMC(object):

    def __init__(self):
        self._centroids = None

    @property
    def centroids(self):
        return self._centroids

    @centroids.setter
    def centroids(self, value):
        self._centroids = value

    def fit(self, x, y):
        """Fitting the NMC classifier to data by
        estimating centroids for each class."""
        n_classes = np.unique(y).size
        n_features = x.shape[1]
        self._centroids = np.zeros(shape=(n_classes, n_features))
        for k in range(n_classes):
            self._centroids[k, :] = x[y == k, :].mean(axis=0)

    def predict(self, x):
        """Predict the class of each input."""
        if self._centroids is None:
            raise ValueError("Run fit first to estimate the centroids")
        dist = euclidean_distances(x, self._centroids, squared=True)
        return np.argmin(dist, axis=1)
