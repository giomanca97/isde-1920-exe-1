import numpy as np
import math
from os import path

from fun_utils import load_mnist, split_data, NMC

# Loading MNIST dataset
mnist_path = path.join(path.dirname(__file__), '..', 'data', 'mnist_data.csv')
x, y = load_mnist(csv_filename=mnist_path)


# TEST split_data(X, y, tr_fraction=0.5)
def test_split_data(Xtr, ytr, Xts, yts, tr_frac, n_samples, n_feats):

    assert type(Xtr) == np.ndarray
    assert type(ytr) == np.ndarray
    assert type(Xts) == np.ndarray
    assert type(yts) == np.ndarray

    assert Xtr.ndim == 2
    assert ytr.ndim == 1
    assert Xtr.ndim == 2
    assert yts.ndim == 1

    tr_size = math.floor(n_samples * tr_frac)
    ts_size = n_samples - tr_size

    assert Xtr.shape[0] == tr_size
    assert ytr.size == tr_size
    assert Xts.shape[0] == ts_size
    assert yts.size == ts_size

    assert Xtr.shape[1] == n_feats
    assert Xts.shape[1] == n_feats


# Rescale data in 0-1
x = x / 255

print("Testing `split_data`...")

x_tr, x_ts, y_tr, y_ts = split_data(x, y)
test_split_data(x_tr, y_tr, x_ts, y_ts, 0.5, x.shape[0], x.shape[1])

# TEST split_data(X, y, fract_tr=0.8)
x_tr, x_ts, y_tr, y_ts = split_data(x, y, fract_tr=0.8)
test_split_data(x_tr, y_tr, x_ts, y_ts, 0.8, x.shape[0], x.shape[1])


# TEST fit(Xtr, ytr)
def test_fit(res, n_c, n_feats):

    assert res.centroids is not None
    assert res.centroids.shape[0] == n_c
    assert res.centroids.shape[1] == n_feats


clf = NMC()

print("Testing `fit`...")

clf.fit(x, y)
test_fit(clf, np.unique(y).size, x.shape[1])


# TEST fit(Xtr, ytr)
def test_predict(res, n_samples):

    assert res.ndim == 1
    assert res.shape[0] == n_samples


print("Testing `predict`...")

y_pred = clf.predict(x_ts)
test_predict(y_pred, x_ts.shape[0])

test_error = (y_ts != y_pred).sum() / y_ts.size
print("Error on test set: ", test_error)

print("All tests successful!")
