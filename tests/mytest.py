import numpy as np
import math
from os import path
import unittest

from fun_utils import load_mnist, split_data, NMC

# Loading MNIST dataset
mnist_path = path.join(path.dirname(__file__), '..', 'data', 'mnist_data.csv')
x, y = load_mnist(csv_filename=mnist_path)

class TestNMC(unittest.TestCase):
    def test_split_data(self, Xtr, ytr, Xts, yts, tr_frac, n_samples, n_feats):

        self.assertEqual(type(Xtr), np.ndarray)
        self.assertEqual(type(ytr), np.ndarray)
        self.assertEqual(type(Xts), np.ndarray)
        self.assertEqual(type(yts), np.ndarray)

        self.assertEqual(Xtr.ndim, 2)
        self.assertEqual(ytr.ndim, 1)
        self.assertEqual(Xtr.ndim, 2)
        self.assertEqual(yts.ndim ,1)

        tr_size = math.floor(n_samples * tr_frac)
        ts_size = n_samples - tr_size

        self.assertEqual(Xtr.shape[0], tr_size)
        self.assertEqual(ytr.size, tr_size)
        self.assertEqual(Xts.shape[0], ts_size) 
        self.assertEqual(yts.size, ts_size)

        self.assertEqual(Xtr.shape[1], n_feats)
        self.assertEqual(Xts.shape[1], n_feats)

    def test_fit(self, res, n_c, n_feats): 

        self.assertIsNotNone(res.centroids)
        self.assertEqual(res.centroids.shape[0], n_c)
        self.assertEqual(res.centroids.shape[1], n_feats)

    def test_predict(self, res, n_samples):

        self.assertEqual(res.ndim, 1)
        self.assertEqual(res.shape[0], n_samples)


tc = TestNMC()
# Rescale data in 0-1
x = x / 255

print("Testing `split_data`...")

x_tr, x_ts, y_tr, y_ts = split_data(x, y)
tc.test_split_data(x_tr, y_tr, x_ts, y_ts, 0.5, x.shape[0], x.shape[1])

# TEST split_data(X, y, fract_tr=0.8)
x_tr, x_ts, y_tr, y_ts = split_data(x, y, fract_tr=0.8)
tc.test_split_data(x_tr, y_tr, x_ts, y_ts, 0.8, x.shape[0], x.shape[1])

clf = NMC()

print("Testing `fit`...")

# TEST fit(Xtr, ytr)
clf.fit(x, y)
tc.test_fit(clf, np.unique(y).size, x.shape[1])

# TEST predict(Xtr, ytr)
print("Testing `predict`...")

y_pred = clf.predict(x_ts)
tc.test_predict(y_pred, x_ts.shape[0])

test_error = (y_ts != y_pred).sum() / y_ts.size
print("Error on test set: ", test_error)

print("All tests successful!")




